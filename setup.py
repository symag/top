from setuptools import setup, find_packages

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='top',
   version='1.1',
   description='Aliexpress API wrapper (Python 3)',
   license="MIT",
   long_description=long_description,
   author='SYMAG team',
   packages=find_packages()
)
