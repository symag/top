'''
Created by auto_sdk on 2019.04.30
'''
from top.api.base import RestApi
class AliexpressSolutionProductSkuInventoryEditRequest(RestApi):
	def __init__(self,domain='gw.api.taobao.com',port=80):
		RestApi.__init__(self,domain, port)
		self.edit_product_sku_inventory_request = None

	def getapiname(self):
		return 'aliexpress.solution.product.sku.inventory.edit'
