'''
Created by auto_sdk on 2018.09.11
'''
from top.api.base import RestApi
class AliexpressSolutionProductSubjectDescriptionEditRequest(RestApi):
	def __init__(self,domain='gw.api.taobao.com',port=80):
		RestApi.__init__(self,domain, port)
		self.edit_product_subject_description_request = None

	def getapiname(self):
		return 'aliexpress.solution.product.subject.description.edit'
